extern crate pgetopts;

use std::env;

#[test]
fn main() {
    pgetopts::Options::new().parse(env::args()).unwrap();
}
