pgetopts
===
[![Build Status](https://travis-ci.org/0X1A/pgetopts.svg)](https://travis-ci.org/0X1A/pgetopts)

[Documentation](http://0x1a.github.io/pgetopts/pgetopts/index.html)

`pgetopts`(pretty getopts) is a  Rust library for option parsing for CLI utilities, a fork of the Rust team's
[getopts](https://github.com/rust-lang/getopts)


# Usage

This crate is [on crates.io](https://crates.io/crates/pgetopts) and can be
used by adding `pgetopts` to the dependencies in your project's `Cargo.toml`.

```toml
[dependencies]
pgetopts = "*"
```

and this to your crate root:

```rust
extern crate pgetopts;
```

# Example

The following example shows simple command line parsing for an application
that requires an input file to be specified, accepts an optional output file
name following `-o`, and accepts both `-h` and `--help` as optional flags.

```{.rust}
extern crate pgetopts;
use pgetopts::Options;
use std::env;

fn do_work(inp: &str, out: Option<String>) {
    println!("{}", inp);
    match out {
        Some(x) => println!("{}", x),
        None => println!("No Output"),
    }
}

fn print_usage(program: &str, opts: Options) {
    print!("{}:", program);
    println!("{}", opts.options());
}

fn main() {
    let args: Vec<String> = env::args().collect();
    let program = args[0].clone();

    let mut opts = Options::new();
    opts.optopt("o", "", "Set output file name", "NAME");
    opts.optflag("h", "help", "Print this help menu");
    let matches = match opts.parse(&args[1..]) {
        Ok(m) => { m }
        Err(f) => { panic!(f.to_string()) }
    };
    if matches.opt_present("h") {
        print_usage(&program, opts);
        return;
    }
    let output = matches.opt_str("o");
    let input = if !matches.free.is_empty() {
        matches.free[0].clone()
    } else {
        print_usage(&program, opts);
        return;
    };
    do_work(&input, output);
}
```

## Display
![Screenshot](screenshot.png)
